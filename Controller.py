class Controller:
  def __init__(self, account):
    self.account = account

  def whoAmI(self):
    print(f"{self.account.username}")

  def checkBalance(self):
    print(f"Your current balance: ${self.account.balance}")
    return self.account.balance

  def deposit(self, amount):
    self.account.balance += amount
    return self.checkBalance()

  def withdraw(self, amount):
    if (amount > self.account.balance):
      print(f"Withdrawing amount excceeded current balance! Please enter a smaller amount.")
    else:    
      self.account.balance -= amount
    return self.checkBalance()
  
