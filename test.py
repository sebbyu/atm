from Bank import Bank
from Account import Account
from Controller import Controller


import unittest

class TestBank(unittest.TestCase):

  def setUp(self):
    self.boa = Bank("BOA")
    self.chase = Bank("CHASE")
    self.account = Account(self.boa)
    self.account.username = "user1"
    self.account.pin = "1234"
    self.account.create(self.account.username, self.account.pin)

  def test_create_bank(self):
    self.assertEqual(self.boa.name, "BOA", "Should be 'BOA'")
    self.assertEqual(self.chase.name, "CHASE", "Should be 'CHASE'")

  def test_insertCard(self):
    self.assertTrue(self.boa.insertCard(self.account.username, self.account.pin), "Should be 'true'")
    self.assertFalse(self.chase.insertCard(self.account.username, self.account.pin), "Should be 'false'")
    self.assertFalse(self.boa.insertCard(self.account.username, "4321"), "Should be 'false'")
    self.assertFalse(self.boa.insertCard("user2", self.account.pin), "Should be 'false'")



class TestAccount(unittest.TestCase):

  def setUp(self):
    self.bank = Bank("BOA")
    self.account = Account(self.bank)
    self.username = "user1"
    self.pin = "1234"
    self.account.create(self.username, self.pin)

  def test_create_account(self):
    self.assertEqual(self.account.bank, self.bank, "Should be 'BOA'")
    self.assertEqual(self.account.balance, 0, "Should be 0")
    self.assertFalse(self.account.authorized, "Should be 'false'")
    self.assertEqual(self.account.username, self.username, "Should be 'user1'")
    self.assertEqual(self.account.pin, self.pin, "Should be '1234'")
  
  def test_authorization(self):
    self.account.create(self.username, self.pin)
    self.account.login(self.username, self.pin)
    self.assertTrue(self.account.authorized, "Should be 'true'")
    
  def test_login(self):
    random_username = "random"
    random_pin = "4321"
    self.assertEqual(self.account.login("random", "4321"), None)

  def test_removeCard(self):
    self.account.login(self.username, self.pin)
    self.assertTrue(self.account.authorized, "Should be 'true'")
    self.account.removeCard()
    self.assertFalse(self.account.authorized, "Should be 'false'")

  def test_deposit(self):
    self.account.login(self.username, self.pin)
    self.account.controller.deposit(50)
    self.assertEqual(self.account.balance, 50, "Should be '50'")
    self.account.controller.deposit(50)
    self.assertEqual(self.account.balance, 100, "Should be '100'")

  def test_withdraw(self):
    self.account.login(self.username, self.pin)
    self.account.controller.deposit(50)
    self.account.controller.withdraw(30)
    self.assertEqual(self.account.balance, 20, "Should be '20'")
    self.account.controller.withdraw(20)
    self.assertEqual(self.account.balance, 0, "Should be '0'")
    self.account.controller.deposit(20)
    self.account.controller.withdraw(30)
    self.assertEqual(self.account.balance, 20, "Should be '20'")

if __name__ == '__main__':
  unittest.main()