from Bank import Bank
from Account import Account


def main():
  bank = Bank("BearBank")
  while True:
    try:
      command = input("Command (press enter for help): ")

      if command == "create" or command == "n":
        username = input("username: ")
        if not bank.alreadyExist(username):
          try:
            pin = int(input("pin: "))
            account = Account(bank)
            account.create(username, pin)
          except ValueError:
            print(f"Your pin must be integers only. Please try again.")



      elif command == "insertCard" or command == "i":
        username = input("Enter your username: ")
        try:
          pin = int(input("pin: "))
          account.login(username, pin)
        except ValueError:
          print(f"Incorrect Pin! Please try again.")



      elif command == "balance" or command == "b":
        if not account.authorized:
          print("Please Login First")
        else:
          account.controller.checkBalance()



      elif command == "deposit" or command == "d":
        if not account.authorized:
          print("Please Login First")
        else:
          try:
            amount = int(input("amount: "))
            account.controller.deposit(amount)
          except ValueError:
            print(f"Your amount must be integers only. Please try again.")



      elif command == "withdraw" or command == "w":
        if not account.authorized:
          print("Please Login First")
        else:
          try:
            amount = int(input("amount: "))
            account.controller.withdraw(amount)
          except ValueError:
            print(f"Your amount must be integers only. Please try again.")



      elif command == "removeCard" or command == "r":
        if not account.authorized:
          print("Please Login First")
        else:
          account.removeCard()


      elif command == "quit" or command == 'q':
        break



      elif command == "" or command == "help":
        print()
        print("create (n) - Create an account with username and pin.")
        print("insertCard (i) - Log in to your ATM account.")
        print("removeCard (r) - Logout from your ATM account.")
        print("balance (b) - Check your current balance.")
        print("deposit (d) - Deposit to your account.")
        print("withdraw (w) - Withdraw from your account.")
        print("quit (q) - Quit.")



      else:
        print(f"'{command}' is an invalid command. Please enter one of the following: ")
        print()
        print("create (n) - Create an account with username and pin.")
        print("insertCard (i) - Log in to your ATM account.")
        print("removeCard (r) - Logout from your ATM account.")
        print("balance (b) - Check your current balance.")
        print("deposit (d) - Deposit to your account.")
        print("withdraw (w) - Withdraw from your account.")
        print("quit (q) - Quit.")
      print()
    except AttributeError:
      print("you need to insert your card and login to your ATM machine")
    except UnboundLocalError:
      print("ERROR. You either do not have an account with us or did not login yet.")

main()