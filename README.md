# ATM Controller

This project is created using Python. It allows users to create a bank account and deposit/withdraw money from it.

### Installation

No requirements need to be installed. Simply clone the project by running the command:

```
git clone https://gitlab.com/sebbyu/atm.git
```
Or simply download the zip file from:
* [https://gitlab.com/sebbyu/atm](https://gitlab.com/sebbyu/atm)

## Getting Started
- After the folder is either cloned or downloaded, run the command:
```
python main.py
```
- It will ask you to input additional command.
- For more info, type "enter" in the terminal while the program's running. It will print out all the available commands.

## Running the tests

- Simply run the command:
```
python test.py
```
- Feel free to add additional tests in **test.py**.

## Built With

* [Python](https://www.python.org/) - The Programming Language

## Author

* **SEBBYU** - [sebbyu](https://gitlab.com/sebbyu)


