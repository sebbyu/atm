class Bank:
  def __init__(self, name):
    self.name = name
    self.accounts = {}
  
  def insertCard(self, username, pin):
    if self.accounts.get(username):
      if self.accounts.get(username) == pin:
        return True
      else:
        print(f"Incorrect Pin! Please try again.")
        return False
    else:
      print(f"ERROR: username {username} does not exist.")
      return False

  def alreadyExist(self, username):
    if username in self.accounts:
      print(f'The username {username} already exists')
      return True
    return False

  def __str__(self):
    return self.name